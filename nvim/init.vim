set tabstop=2
set softtabstop=2
set shiftwidth=0
set smarttab
set autoindent
set t_Co=256
set smartindent
set number
set wrap
syntax on
set encoding=utf-8
set mouse=a
set keymap=russian-jcukenwin
set iminsert=0
set imsearch=0
set cursorline
set cursorcolumn
set t_md=
set ffs=unix,dos,mac
setlocal swapfile
set clipboard+=unnamedplus
set exrc
set secure
set cmdheight=1

let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1

let g:ruby_host_prog = '/home/Zakhar/.gem/ruby/2.6.0/bin/neovim-ruby-host'
colorscheme vimbrant
let g:airline_theme = 'ouo'

nmap <space>w :w<cr>
nmap <space>2 :%!xxd<cr>
nmap <leader>w I<!-- <esc>$a --><esc>0
nmap <leader>z :w<cr> 
nmap <leader>p :bp<cr>
nmap <leader>n :bn<cr>
nmap <leader>1 :ls<cr>
imap № #
imap ( ()<left>
imap [ []<left>
imap {<cr> {<cr>}<esc>O
nmap & O<esc>




"let g:user_emmet_mode='n'    "only enable normal mode functions.
"let g:user_emmet_mode='inv'  "enable all functions, which is equal to
"let g:arduino_dir = '/usr/share/arduino/'
"let g:arduino_home_dir = $HOME."Arduino"
"
"let g:loaded_python2_provider = 0 
"
"let g:deoplete#enable_at_startup = 1
"
"let g:NERDTreeDirArrowExpandable = '>'
"let g:NERDTreeDirArrowCollapsible = '^'
"
"set expandtab
"set nofoldenable
"set foldmethod=syntax
"set noshowmode 
"autocmd InsertLeave, CompleteDone * if pumvisible () == 0 | pclose | endif
"inoremap <expr> <tab> pumvisible ()? "\ <c-n>": "\ <tab>" 
"set laststatus=3 " Always display the statusline in all windows
"set showtabline=3 " Always display the tabline, even if there is only one tab
"set packpath=~/.config/nvim/pack/emmet-vim

"set undodir=~/.config/nvim/undodir/
"set undofile
