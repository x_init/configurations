# If not running interactively, don't do anything
[[ $- != *i* ]] && return


PS1='\[\033[01;31m\]\u\[\033[00m\]\[\033[00;32m\]@\h\[\033[00m\] \w\n\[\033[00;32m\]#\[\033[00m\] '
export PS1
export PATH="$PATH:$HOME/.rvm/bin"
export HISTSIZE=10000
export HISTIGNORE="ls:ps:history"
